<?php

$dirname = dirname(__FILE__);

require_once "{$dirname}/vendor/autoload.php";

$lang     = 'fr-fr';
$source   = 'http://www.ot-montpellier.fr/index.php?pg=direct&p1=actualites-rss&p2=agenda';
$email    = 'contact@ccoste.fr (Charles-Edouard Coste)';
$self     = 'http://rss.ccoste.fr/ot-montpellier';
$content  = file_get_contents($source);
$md5      = md5($content);
$ctagfile = "{$dirname}/ctag";
$ctag     = file_get_contents($ctagfile);
$output   = "{$dirname}/www/ot-montpellier.rss";

// If nothing changed, we skip this script
if ($ctag == $md5) {
    exit();
}

file_put_contents($ctagfile, $md5);

$input = ezcFeed::parseContent($content);

// On crée un nouvel objet DOM
$dom = new DOMDocument();
$dom->formatOutput = true;

// La racine de l'objet DOM est un élément RSS2
$root = $dom->appendChild(
    $dom->createElement("rss")
);
$root->setAttribute("version", "2.0");

// On crée l'élément channel que l'on considère comme la racine
// pour le reste du script
$root = $root->appendChild($dom->createElement("channel"));

// Ajout d'un lien "self" Atom, pour des questions d'intéropérabilité
$node =  $root->appendChild(
    $dom->createElementNS('http://www.w3.org/2005/Atom', 'atom:link')
);
$node->setAttribute('href', $self);
$node->setAttribute('rel', 'self');
$node->setAttribute('type', 'application/rss+xml');

// Ajout du titre du channel
$node = $root->appendChild(
    $dom->createElement("title")
);
$node = $node->appendChild(
    new DOMCdataSection($input->title)
);

// Ajout de la date de dernière génération du fichier (maintenant) au format RFC 2822
$node = $root->appendChild(
    $dom->createElement("lastBuildDate", date("r"))
);

// Lien vers le flux RSS que nous générons
$root->appendChild(
    $dom->createElement("link", $self)
);

// RSS feed description
$node = $root->appendChild(
    $dom->createElement("description")
);

// Protect content from parsers
$node->appendChild(
    new DOMCdataSection($input->description)
);

// Set language
$root->appendChild(
    $dom->createElement("language", $lang)
);

// For legal purpose, I add my email
$root->appendChild(
    $dom->createElement("webMaster", $email)
);

foreach ( $input->item as $input_item )
{
    $item = $root->appendChild(
        $dom->createElement("item")
    );

    $node = $item->appendChild(
        $dom->createElement("title")
    );
    $node->appendChild(
        new DOMCdataSection("{$input_item->title}")
    );

    $node = $item->appendChild(
        $dom->createElement("description")
    );

    $node->appendChild(
        new DOMCdataSection("{$input_item->description}")
    );

    $node = $item->appendChild(
        $dom->createElement("link", "{$input_item->link[0]->href}")
    );

    // Le GUID est l'identificateur unique d'un item
    // Il est très souvent identique au `link`
    $node = $item->appendChild(
        $dom->createElement("guid", "{$input_item->id}")
    );

    /**
     * Le flux RSS que nous souhaitons corriger n'indique pas la
     * taille des fichiers liés. Il nous faut donc la retrouver
     * au moyen de requêtes HEAD via get_headers() en définissant
     * un contexte de flux pour que la requête envoyée soit une
     * requête HEAD et non pas GET.
     * Cela évite d'avoir à télécharger le fichier pour connaître
     * sa taille.
     */
    stream_context_set_default(
        array(
            'http' => array(
                'method' => 'HEAD'
            )
        )
    );

    foreach ($input_item->enclosure as $enclosure) {

        $node = $item->appendChild(
            $dom->createElement("enclosure")
        );

        $node->setAttribute('url', $enclosure->url);
        $node->setAttribute('type', $enclosure->type);

        $data = get_headers($enclosure->url, true);
        $node->setAttribute('length', (int) $data['Content-Length']);
    }
}

file_put_contents($output, $dom->saveXML());
