ot-montpellier-rss-correction
=============================

Cela fait plus de 5 ans que j'attends que l'Office de Tourisme de Montpellier corrige son flux RSS. J'ai donc décidé de me monter un proxy correcteur et de fournir le code source au cas où ça puisse inspirer quelqu'un...

Comment ça fonctionne?
----------------------

- Un script cronjob récupère le contenu du [flux RSS de l'office de tourisme](http://www.ot-montpellier.fr/index.php?pg=direct&p1=actualites-rss&p2=agenda)
- Ce dernier applique les corrections nécessaires
- Le script sauvegarde le résultat dans un fichier XML public

Quels sont les points corrigés?
-------------------------------

### Suppression des `pubDate`

Une erreur assez fréquente sur certains sites web est de vouloir utiliser des flux RSS au lieu de fichiers iCalendar.
Les balises `pubDate` de RSS sont là pour indiquer quand une publication a eu lieu et non pas quand un événement se produira.

Le flux RSS que nous souhaitons corriger utilise ces balises pour donner la date de début d'un événement. Cela n'a aucun sens d'autant plus qu'un lecteur RSS pourrait ne pas afficher les éléments dont les dates de publication sont dans le futur.

Tant que le contenu de ces balises n'est pas remplacé par une donnée sémantiquement correcte, il vaut mieux les supprimer tout bonnement.

### Correction des `enclosure`

Les balises enclosure n'indiquent pas la taille des éléments liés. Ce script effectue quelques requêtes auprès du serveur qui héberge la ressource pour lui demander la taille de cet élément.

Voir le résultat
----------------

Ce code est en ligne et fonctionne sur http://rss.ccoste.fr/ot-montpellier.rss

Licence
=======

[![AGPL](http://www.gnu.org/graphics/agplv3-155x51.png)](http://www.gnu.org/licenses/agpl-3.0.html)

Copyright (C) 2014 Charles-Edouard Coste

Ce programme est un logiciel libre: vous pouvez le redistribuer et/ou
le modifier sous les termes de la licence `GNU Affero General Public Licence`
tels que publiés par la Free Software Foundation dans la version 3
de cette licence, ou (à votre convenance) dans n'importe quelle version ultérieure

Ce programme est distribué dans l'espoir d'être utile, mais 
SANS AUCUNE GARANTIE. Veuillez vous référer à la licence `GNU Affero General Public Licence`
pour plus de détails.

Vous devriez avoir reçu une copie de la `GNU Affero General Public Licence`
avec ce programme. Si ce n'est pas le cas, rendez-vous sur <http://www.gnu.org/licenses/>.
